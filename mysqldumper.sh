#!/bin/bash

SCRIPT_NAME=mysqldumper
## Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT_FULLPATH=$(readlink -f "$0")

## Absolute path this script is in, thus /home/user/bin
SCRIPT_DIR=$(dirname "${SCRIPT_FULLPATH}")

CONFIGFILE_NAME="${SCRIPT_DIR}/${SCRIPT_NAME}.conf"

if [ ! -f "${CONFIGFILE_NAME}" ]; then
  echo "Config file '${CONFIGFILE_NAME}' not found" >&2
  exit 1
fi

source "${CONFIGFILE_NAME}"

LP="--login-path=\"${MYSQL_LOGIN_PATH}\""

# retrieve databases list 
DATABASES=$(mysql ${LP} -Ns -e 'show databases')

if [ "$?" -ne "0" ]; then 
	echo "list data error!" >&2
	exit 1
fi

## Exclude filter
if [ ! -z "$MYSQL_DB_EXCLUDE" ]; then
	DATABASES=$(echo "${DATABASES}" | egrep -vx "${MYSQL_DB_EXCLUDE}")
fi

# create backup folder
BACKUP_BASE_DIR="${MYSQL_BACKUP_DIR}/${MYSQL_HOST}"
if [ ! -d "${BACKUP_BASE_DIR}" ]; then 
  mkdir -p "${BACKUP_BASE_DIR}"
fi

echo "Exporting to ${BACKUP_BASE_DIR}"
pushd "${BACKUP_BASE_DIR}" >/dev/null

## Iterate over DBs
counter=0
for DATABASE in $DATABASES; do
	counter=$((counter+1))
	## Dump filename
	DUMPFILE_FULLPATH="${DATABASE}.sql"
	
	## mysqldump
  	echo -e "[$counter] > ${DATABASE}"
	mysqldump ${LP} ${MYSQLDUMP_OPTIONS} --databases "${DATABASE}" --result-file="${DUMPFILE_FULLPATH}"

	## autocommit optimization - header
	NO_AUTOCOMMIT_TEXT="SET @OLD_AUTOCOMMIT=@@AUTOCOMMIT, AUTOCOMMIT=0;"
	sed -i "/Server version/ a ${NO_AUTOCOMMIT_TEXT}" "$DUMPFILE_FULLPATH"

	## autocommit optimization - footer
	echo "COMMIT;" >> "${DUMPFILE_FULLPATH}"
	echo "SET AUTOCOMMIT=@OLD_AUTOCOMMIT" >> "${DUMPFILE_FULLPATH}"
	
	## compression
	rm -f "${DUMPFILE_FULLPATH}.tar.gz"
  tar czf ${DUMPFILE_FULLPATH}.tar.gz "${DUMPFILE_FULLPATH}"
  rm -f ${DUMPFILE_FULLPATH}
done

# restore dir
popd >/dev/null

